import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';

class ConfigReader {
  static Future<Config> readMusicList() async {
    String jsonString = await rootBundle.loadString("config/config.json");
    final jsonResult = json.decode(jsonString) as Map<String, dynamic>;
    final config = Config(defaultPath: jsonResult["defaultPath"] as String);
    return config;
  }
}

class Config {
  
  final String defaultPath;

  Config({required this.defaultPath});
}