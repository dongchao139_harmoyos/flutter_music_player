class TagCategory {
  final String categoryName;
  final List<Tag> tags;

  TagCategory(this.tags, {required this.categoryName});
}

class Tag {
  final int tagId;
  final String tagName;

  Tag({required this.tagId, required this.tagName});
}

class Playlist {
  final int playlistId;
  final String coverImgUrl;
  final String playlistTitle;
  final String playedCount;

  Playlist({required this.playlistId, required this.coverImgUrl, required this.playlistTitle, required this.playedCount});
}

class Song {
  final String title;
  final String linkUrl;
  final String imgUrl;
  final String summary;
  final String singer;
  final String album;

  Song({required this.title, required this.linkUrl, required this.imgUrl, required this.summary, required this.singer, required this.album});
}


class Board {
  final String name;
  final String url;

  Board({required this.name, required this.url});
}

abstract class PlaylistApi {

  Future<List<TagCategory>> requestTags();

  Future<List<Playlist>> requestPlaylistList(int tagId);

  Future<List<Song>> requestSongList(int playlistId);

  Future<List<Board>> requestBoardList();

  Future<List<Song>> requestSongListByBoard(String boardUrl);

  Future<String> qrLogin();

  Future<String> requestPlayInfoAndPlay({required String copyrightId});
}
