import 'dart:convert';
import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/foundation.dart';
import 'package:html/dom.dart';
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as html_paser;
import '../api.dart';
import './encrypt.dart';

class MiguPlaylistApi extends PlaylistApi {
  bool login = false;
  List<TagCategory>? _tagCategories;
  Map<int, List<Playlist>> playlistMap = {};
  Map<int, List<Song>> songListMap = {};
  Map<String, List<Song>> boardSongListMap = {};
  String _cookie = "audioplayer_exist=1; mg_uem_user_id_9fbe6599400e43a4a58700a822fd57f8=551791c4-c667-45bc-83ca-03defc46aa42; cookieId=Th-81XRAa6bD9FPYhfTk_gkch5c1W4h1705221576370;";

  late AudioPlayer player;
  void Function() loginCallback = () => {};
  Map<String, String> playUrlMap = {};

  Future<Document?> requestDocument(String uri) async {
    var url = Uri.parse(uri);
    var response = await http.get(url);
    if (response.statusCode != 200) {
      printError(response.statusCode);
      return null;
    }
    var document = html_paser.parse(response.body);
    return document;
  }

  @override
  Future<List<TagCategory>> requestTags() async {
    if (_tagCategories != null) {
      return _tagCategories!;
    }
    List<TagCategory> tagCategories = [];
    var document =
        await requestDocument("https://music.migu.cn/v3/music/playlist");
    if (document == null) {
      return [];
    }
    var tagNames = document.querySelectorAll(".tag-name");
    for (var tagDiv in tagNames) {
      Map<int, Tag> tags = {};
      var ptagNormal = tagDiv.nextElementSibling;
      if (ptagNormal != null) {
        var aList = ptagNormal.querySelectorAll('a');
        for (var aElement in aList) {
          var tagName = aElement.innerHtml; // 小清新
          if (aElement.attributes.containsKey('href')) {
            var tagIdStr =
                aElement.attributes['href']!.split('?'); // tagId=1000001635
            if (tagIdStr.length > 1 && tagIdStr[1].startsWith("tagId")) {
              var tagId = int.parse(tagIdStr[1].split('=')[1]);
              var tag = Tag(tagId: tagId, tagName: tagName);
              tags[tagId] = tag;
            }
          }
        }
      }
      tagCategories.add(
          TagCategory(tags.values.toList(), categoryName: tagDiv.innerHtml));
    }
    _tagCategories = tagCategories;
    return tagCategories;
  }

  @override
  Future<List<Playlist>> requestPlaylistList(int tagId) async {
    if (playlistMap.containsKey(tagId)) {
      return playlistMap[tagId]!;
    }
    List<Playlist> playlistList = [];
    var document = await requestDocument(
        "https://music.migu.cn/v3/music/playlist?tagId=$tagId");
    if (document == null) {
      return [];
    }
    var container = document.querySelector(".song-list-cont");
    if (container != null) {
      var liList = container.querySelectorAll('li');
      for (var li in liList) {
        var i = li.querySelector("i.cf-fenxiang-big");
        if (i != null) {
          var dataShareStr = i.attributes['data-share']!;
          var json = jsonDecode(dataShareStr) as Map<String, dynamic>;

          var playlistUrl = json['linkUrl'];
          playlistUrl = playlistUrl.substring(playlistUrl.lastIndexOf('/') + 1);
          var playlistId =
              playlistUrl.substring(0, playlistUrl.lastIndexOf('?'));
          var playedCount =
              li.querySelector('div.desc-text')!.nodes[2].text!.trim();

          var playlist = Playlist(
              playlistId: int.parse(playlistId),
              coverImgUrl: json['imgUrl'],
              playlistTitle: json['title']!,
              playedCount: playedCount);
          playlistList.add(playlist);
        }
      }
    }
    playlistMap[tagId] = playlistList;
    return playlistList;
  }

  @override
  Future<List<Song>> requestSongList(int playlistId) async {
    if (songListMap.containsKey(playlistId)) {
      return songListMap[playlistId]!;
    }
    List<Song> songList = [];
    var document = await requestDocument(
        "https://music.migu.cn/v3/music/playlist/$playlistId");
    if (document == null) {
      return [];
    }
    var rows = document.querySelectorAll("div.row");
    for (var row in rows) {
      var dataShareA = row.querySelector('a.J-btn-share');
      if (dataShareA != null) {
        var dataShareStr = dataShareA.attributes['data-share']!;
        var json = jsonDecode(dataShareStr) as Map<String, dynamic>;
        var song = Song(
          title: json['title'] as String,
          linkUrl: json['linkUrl'] as String,
          imgUrl: json['imgUrl'] as String,
          summary: json['summary'] as String,
          singer: json['singer'] as String,
          album: json['album'] as String,
        );
        songList.add(song);
      }
    }
    songListMap[playlistId] = songList;
    return songList;
  }

  @override
  Future<List<Board>> requestBoardList() async {
    return [
      Board(name: "尖叫新歌榜", url: "/v3/music/top/jianjiao_newsong"),
      Board(name: "尖叫热歌榜", url: "/v3/music/top/jianjiao_hotsong"),
      Board(name: "尖叫原创榜", url: "/v3/music/top/jianjiao_original"),
      Board(name: "影视榜", url: "/v3/music/top/movies"),
      Board(name: "内地榜", url: "/v3/music/top/mainland"),
      Board(name: "港台榜", url: "/v3/music/top/hktw"),
      Board(name: "欧美榜", url: "/v3/music/top/eur_usa"),
      Board(name: "日韩榜", url: "/v3/music/top/jpn_kor"),
      Board(name: "彩铃榜", url: "/v3/music/top/coloring"),
      Board(name: "KTV榜", url: "/v3/music/top/ktv"),
      Board(name: "网络榜", url: "/v3/music/top/network"),
      Board(name: "新专辑榜", url: "/v3/music/top/newalbum"),
      Board(name: "MV榜", url: "/v3/music/top/mv"),
    ];
  }

  @override
  Future<List<Song>> requestSongListByBoard(String boardUrl) async {
    if (boardSongListMap.containsKey(boardUrl)) {
      return boardSongListMap[boardUrl]!;
    }
    var url = Uri.parse("https://music.migu.cn$boardUrl");
    var response = await http.get(url);
    if (response.statusCode != 200) {
      printError(response.statusCode);
      return [];
    }
    List<Song> songList = [];
    var listDataJson = response.body.substring(response.body.indexOf('var listData = ') + 15, response.body.indexOf("top-content"));
    listDataJson = listDataJson.substring(0, listDataJson.indexOf('</script>'));
    var listData = jsonDecode(listDataJson) as Map<String, dynamic>;
    if (!listData.containsKey('songs')) {
      return [];
    }
    var songs = listData['songs'] as Map<String, dynamic>;
    for (Map<String, dynamic> item in songs['items']) {
      Map<String, dynamic>? album;
      if (item.containsKey('album')) {
        album = item['album'];
      }
      var singers = item['singers'] as List<dynamic>;
      songList.add(Song(
        title: item['name'] as String,
        linkUrl: "/v3/music/song/${item['copyrightId']}",
        imgUrl: "https:${item["mediumPic"]}",
        summary: item['name'] as String,
        singer: singers.map((e) => e['name']).join('/'),
        album: album == null ? '' : album['albumName'],
      ));
    }
    boardSongListMap[boardUrl] = songList;
    return songList;
  }

  @override
  Future<String> qrLogin() async {
    String sourceID = "220001";
    var url = Uri.parse("https://passport.migu.cn/api/qrcWeb/qrcLogin?sourceID=$sourceID");
    var headerMap = {
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      "Referer": "https://passport.migu.cn/login?sourceid=$sourceID&apptype=0&forceAuthn=false&isPassive=false&authType=MiguPassport&passwordControl=0&display=web&referer=https://music.migu.cn/&logintype=1&qq=null&weibo=null&alipay=null&weixin=null&andPass=null&phoneNumber=&callbackURL=https%3A%2F%2Fmusic.migu.cn%2Fv3%2Fmusic%2Fplayer%2Faudio&relayState=&openPage=&hideRegister=&hideForgetPass=&sim=&needOneKey=0&hideps=0&hideqrc=0&hidetitle=0",
    };
    var body = "isAsync=true&sourceid=$sourceID";
    var response = await http.post(url, headers: headerMap, body: body);
    if (response.statusCode != 200) {
      printError(response.statusCode);
      return "";
    }
    var json = jsonDecode(response.body) as Map<String, dynamic>;
    var result = json['result'] as Map<String, dynamic>;

    var qrcUrl = result['qrcUrl'] as String; // data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgG
    // var qrcWebtype = result['qrcWebtype'] as String; // 1
    var qrcSessionid = result['qrc_sessionid'] as String; // 8f5a353a9838d120948a98b820b6772a
    Timer? updater;

    updater = Timer.periodic(const Duration(seconds: 1), (timer) async {
      var qrcodeResponseBody = await qrcquery(sourceID: sourceID, qrcSessionid: qrcSessionid);
      if (updater != null && qrcodeResponseBody.indexOf("登陆成功") > 0) {
        updater.cancel();
        var responseJson = jsonDecode(qrcodeResponseBody) as Map<String, dynamic>;
        var resultJson = responseJson['result'] as Map<String, dynamic>;
        var token = resultJson['token'] as String;
        await qrDoLogin(token);
        loginCallback();
        login = true;
      }
    });
    return qrcUrl.substring("data:image/jpeg;base64,".length);
  }

  Future<String> qrcquery({required String sourceID, required String qrcSessionid}) async {
    var url = Uri.parse("https://passport.migu.cn/api/qrcWeb/qrcquery");
    var headerMap = {
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      "Referer": "https://passport.migu.cn/login?sourceid=$sourceID&apptype=0&forceAuthn=false&isPassive=false&authType=MiguPassport&passwordControl=0&display=web&referer=https://music.migu.cn/&logintype=1&qq=null&weibo=null&alipay=null&weixin=null&andPass=null&phoneNumber=&callbackURL=https%3A%2F%2Fmusic.migu.cn%2Fv3%2Fmusic%2Fplayer%2Faudio&relayState=&openPage=&hideRegister=&hideForgetPass=&sim=&needOneKey=0&hideps=0&hideqrc=0&hidetitle=0",
    };
    var body = "isAsync=true&sourceid=$sourceID&qrc_sessionid=$qrcSessionid";
    var response = await http.post(url, headers: headerMap, body: body);
    if (response.statusCode != 200) {
      printError(response.statusCode);
      return "";
    }
    return response.body;
  }

  Future<String> qrDoLogin(String token) async {
    var headerMap = {
      "Cookie": _cookie,
      "Referer": "https://music.migu.cn/v3/music/player/audio",
    };
    String uri = "https://music.migu.cn/v3/user/login?callbackURL=https%3A%2F%2Fmusic.migu.cn%2Fv3%2Fmusic%2Fplayer%2Faudio&relayState=&token=$token&qrclogin=1&logintype=QRCSSO";
    var url = Uri.parse(uri);
    var response = await http.get(url, headers: headerMap);
    if (response.statusCode != 200) {
      printError(response.statusCode);
      return "";
    }
    var setCookie = response.headers['set-cookie'] as String;
    _cookie = "";
    for (var cook in setCookie.split("migu_music")) {
      if (cook.isEmpty) {
        continue;
      }
      cook = "migu_music${cook.substring(0, cook.indexOf(";"))}; ";
      _cookie = _cookie + cook;
    }
    return response.body;
  }

  @override
  Future<String> requestPlayInfoAndPlay({required String copyrightId}) async {
    if (!login) {
      return "";
    }
    if (playUrlMap.containsKey(copyrightId) && playUrlMap[copyrightId] != null) {
      player.play(UrlSource(playUrlMap[copyrightId]!));
      return playUrlMap[copyrightId]!;
    }
    // 点歌曲播放时，获取参数copyrightId
    var encryptedData = encrypt(copyrightId: copyrightId);
    var data = Uri.encodeQueryComponent(encryptedData.data);
    var secKey = Uri.encodeQueryComponent(encryptedData.secKey);
    var playUrl = await requestPlayInfo(copyrightId: copyrightId, dataType: encryptedData.dataType, data: data, secKey: secKey);
    playUrlMap[copyrightId] = playUrl;
    player.play(UrlSource(playUrl));
    return playUrl;
  }

  Future<String> requestPlayInfo({required String copyrightId, required int dataType, required String data, required String secKey}) async {
    var url = Uri.parse("https://music.migu.cn/v3/api/music/audioPlayer/getPlayInfo?dataType=$dataType&data=$data&secKey=$secKey");
    var headerMap = {
      "Cookie": _cookie,
      "Referer": "https://music.migu.cn/v3/music/player/audio",
    };
    var response = await http.get(url, headers: headerMap);
    if (response.statusCode != 200) {
      printError(response.statusCode);
      return "";
    }
    var json = jsonDecode(response.body) as Map<String, dynamic>;
    if (json['data'] == null) {
      var msg = json['msg'] as String;
      if (kDebugMode) {
        print("msg:$msg");
      }
      return msg;
    }
    var dataJson = json['data'] as Map<String, dynamic>;
    var playUrl = "http:${dataJson["playUrl"]}";
    return playUrl;
  }

  void printError(int statusCode) {
    if (kDebugMode) {
      print('Request failed with status: $statusCode.');
    }
  }
}
