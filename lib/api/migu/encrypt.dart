import 'dart:convert';
import 'dart:math';

import 'package:crypto_dart/crypto_dart.dart';
import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/asymmetric/api.dart';

class EncryptedData {
  
  final int dataType;
  final String data;
  final String secKey;

  EncryptedData({required this.dataType, required this.data, required this.secKey});
}

const publicKey =  
"""-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8asrfSaoOb4je+DSmKdriQJKW
VJ2oDZrs3wi5W67m3LwTB9QVR+cE3XWU21Nx+YBxS0yun8wDcjgQvYt625ZCcgin
2ro/eOkNyUOTBIbuj9CvMnhUYiR61lC1f1IGbrSYYimqBVSjpifVufxtx/I3exRe
ZosTByYp4Xwpb1+WAQIDAQAB
-----END PUBLIC KEY-----""";

EncryptedData encrypt({required String copyrightId, int type = 1, int auditionsFlag = 0}) {
  var e = '{"copyrightId":"$copyrightId","type":$type,"auditionsFlag":$auditionsFlag}';

  var t = Random().nextDouble() * 1000;
  var n = sha256.convert(utf8.encode(t.toString())).toString().substring(0, 32);

  // 将n用JSEncrypt非对称加密得到secKey
  var publicKeyParser = RSAKeyParser().parse(publicKey) as RSAPublicKey;
  final i = Encrypter(RSA(publicKey: publicKeyParser));
  var a = i.encrypt(n).base64;

  // 用AES对称加密(使用和CryptoJS默认相同算法)，使用n作为密钥，对e进行加密得到data
  final r = CryptoDart.AES.encrypt(e, n).toString();

  return EncryptedData(dataType: 2, data: r, secKey: a);
}

