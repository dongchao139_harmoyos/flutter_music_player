import 'dart:io';

import 'package:audioplayers/audioplayers.dart';

import '../api/migu/api.dart';

enum PlayingMode {
  sequence,
  random,
  cycleOne,
}

class MusicModel {
  MusicModel({
    required this.musicName,
    required this.imageUrl,
    required this.musicUrl,
    this.file,
  });
  final FileSystemEntity? file;
  final String musicName;
  final String imageUrl;
  final String musicUrl;
  Duration totalTime = const Duration(seconds: 299);
}  

class MusicListModel {
  MusicListModel({
    required this.api,
    required this.musicList,
    required this.current,
    required this.mode,
    required this.duration,
    required this.directoryPath,
    required this.player,
  }) {
    api.player = player;
  }
  List<MusicModel> musicList;
  int current;
  Duration duration;
  PlayingMode mode;
  String directoryPath;
  final MiguPlaylistApi api;
  final AudioPlayer player;

  MusicListModel clone() {
    return MusicListModel(
        api: api,
        musicList: musicList,
        current: current,
        mode: mode,
        duration: duration,
        directoryPath: directoryPath,
        player: player);
  }
}
