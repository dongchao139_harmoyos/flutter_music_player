
import 'dart:io';

import '../config.dart';

class MusicLiric {
  MusicLiric({
    required this.title,
    required this.author,
    required this.albumn,
    this.composition,
    required this.items,
  });
  final String title; // ti:
  final String author;// ar:
  final String albumn;// al:
  final String? composition;//co:
  final List<MusicLiricItem> items;

  static Future<MusicLiric> parse(String liricFileName) async {
    String title = '';
    String author = '';
    String albumn = '';
    String composition = '';
    List<MusicLiricItem> items = [];
    final config = await ConfigReader.readMusicList();

    File file = File("${config.defaultPath}\\lrc\\$liricFileName");
    var lines = await file.readAsLines();
    for (var line in lines) {
      var split = line.split(']');
      if (split[1] == "") {
        var metaInfo = split[0].replaceAll('[', '');
        var key = metaInfo.split(":")[0];
        var value = metaInfo.split(":")[1];
        if (key == 'ti') {
          title = value;
        } else if (key == 'ar') {
          author = value;
        } else if (key == 'al') {
          albumn = value;
        } else if (key == 'co') {
          composition = value;
        }
        continue;
      }
      String text = '';
      String enterStr = split[0].replaceAll('[', ''); // 01:20.21
      String startStr = enterStr;
      text = split[1];
      if (split.length == 3) {
        startStr = split[1].replaceAll('[', ''); // 01:29.20
        text = split[2];
      }
      Duration enter = Duration(milliseconds: parseMilliseconds(enterStr));
      Duration start = Duration(milliseconds: parseMilliseconds(startStr));
      var item = MusicLiricItem(enter: enter, start: start, text: text);
      items.add(item);
    }
    return MusicLiric(title: title, author: author, albumn: albumn, composition: composition, items: items);
  }

  static int parseMilliseconds(String str) {
    String minute = str.split(':')[0]; // 01
    String seconds = str.split(':')[1].split('.')[0]; // 29
    String miliseconds = "${str.split(':')[1].split('.')[1]}0"; // 200
    int totalMilliseconds = int.parse(miliseconds) + int.parse(seconds) * 1000 + int.parse(minute) * 60 * 1000;
    return totalMilliseconds;
  }
}

class MusicLiricItem {
  MusicLiricItem({
    required this.enter,
    required this.start,
    required this.text,
  });
  Duration enter;
  Duration start;
  String text;
}
