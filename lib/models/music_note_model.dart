class MusicNoteModel {
  final int playlistId;
  final String imageUrl;
  final String title;
  final String author;
  final DateTime date;
  final String startAmount;
  final int playAmount;

  MusicNoteModel(
    this.playlistId, 
    {
      required this.imageUrl,
      required this.title,
      required this.author,
      required this.date,
      required this.startAmount,
      required this.playAmount
    });
}

class MusicNoteTagCategory {
  final String categoryName;
  final List<String> tags;

  MusicNoteTagCategory(this.categoryName, this.tags);
}
