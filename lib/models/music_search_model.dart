class MusicSearchModel {
  final String musicName;
  final String author;
  final String albumn;
  final String musicLength;
  final String? imageUrl;

  MusicSearchModel({
    required this.musicName,
    required this.author,
    required this.albumn,
    required this.musicLength,
    required this.imageUrl,
  });
}
