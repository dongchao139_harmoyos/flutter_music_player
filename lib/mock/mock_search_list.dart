import '../models/music_search_model.dart';

List<MusicSearchModel> musics = [
  MusicSearchModel(
    musicName: "星星点灯",
    author: "郑智化",
    albumn: "星星点灯",
    musicLength: "05:02",
    imageUrl: null,
  ),
  MusicSearchModel(
    musicName: "星星点灯",
    author: "老鬼",
    albumn: "星星点灯",
    musicLength: "04:47",
    imageUrl: null,
  ),
  MusicSearchModel(
    musicName: "星星点灯(郑智化)",
    author: "欣欣快跑",
    albumn: "星星点灯郑智化,星星点灯王心凌",
    musicLength: "04:49",
    imageUrl: null,
  ),
];
