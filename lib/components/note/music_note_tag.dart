import 'package:flutter/material.dart';

import '../../api/api.dart';

class TagItem extends StatefulWidget {
  const TagItem({
    super.key,
    required this.t,
    required this.onSelect,
  });

  final Tag t;
  final void Function(Tag) onSelect;

  @override
  State<TagItem> createState() => _TagItemState();
}

class _TagItemState extends State<TagItem> {
  var color = Colors.blue[300];
  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.only(left: 8, right: 8, top: 5, bottom: 5),
      margin: const EdgeInsets.all(10),
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        onHover: (event) {
          setState(() {
            color = Colors.blueGrey;
          });
        },
        onExit: (event) {
          setState(() {
            color = Colors.blue[300];
          });
        },
        child: GestureDetector(
          onTap: () {
            widget.onSelect(widget.t);
          },
          child: Text(
            widget.t.tagName,
            style: const TextStyle(
              fontSize: 14,
              color: Colors.white,
              fontWeight: FontWeight.normal,
              decoration: TextDecoration.none,
              fontFamily: "思源黑体",
            ),
          ),
        ),
      ),
    );
  }
}
