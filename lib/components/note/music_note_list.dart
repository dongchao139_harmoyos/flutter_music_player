import 'package:flutter/material.dart';

import '../../models/music_note_model.dart';
import 'music_note_item.dart';

// 歌单列表
class MusicNoteList extends StatelessWidget {
  const MusicNoteList({
    super.key,
    required this.musicNoteList,
    required this.onTapItem,
    required this.padding,
  });

  final List<MusicNoteModel> musicNoteList;
  final Function(MusicNoteModel) onTapItem;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 2.5,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
        ),
        itemCount: musicNoteList.length,
        itemBuilder: (context, index) {
          var item = musicNoteList[index];
          return MusicNoteItem(
            item: item,
            onTapItem: () => onTapItem(item),
          );
        },
      ),
    );
  }
}
