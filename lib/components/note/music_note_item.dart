import 'package:flutter/material.dart';

import '../../models/music_note_model.dart';

// 歌单item
class MusicNoteItem extends StatelessWidget {
  const MusicNoteItem({
    super.key,
    required this.item,
    required this.onTapItem,
  });

  final MusicNoteModel item;
  final VoidCallback onTapItem;

  @override
  Widget build(BuildContext context) {
    var style = TextStyle(fontSize: 12, color: Theme.of(context).primaryColor);
    return Row(
      children: [
        MouseRegion(
          cursor: SystemMouseCursors.click,
          child: GestureDetector(
            onTap: onTapItem,
            child: Container(
              clipBehavior: Clip.hardEdge,
              decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5))),
              child: Image.network(
                item.imageUrl,
                width: 130,
              ),
            ),
          ),
        ),
        Container(
          width: 190,
          padding: const EdgeInsets.only(left: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.title,
                softWrap: true,
                textAlign: TextAlign.left,
                style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: Text(
                  item.author,
                  style: style,
                ),
              ),
              Text(
                "${item.date.year}-${item.date.month}-${item.date.day}",
                style: style,
              ),
              const Expanded(child: Text("")),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: Row(
                  children: [
                    Icon(
                      Icons.music_note,
                      size: 14,
                      color: Colors.grey[500]!,
                    ),
                    Text(
                      item.startAmount.toString(),
                      style: style,
                    ),
                    const Text("  "),
                    Icon(
                      Icons.earbuds,
                      size: 14,
                      color: Colors.grey[500]!,
                    ),
                    Text(
                      item.playAmount.toString(),
                      style: TextStyle(fontSize: 12, color: Colors.grey[500]!),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
