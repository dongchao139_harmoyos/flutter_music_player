import 'package:flutter/material.dart';

import '../../api/api.dart';
import 'music_note_tag.dart';

// 歌单 - 标签选择
class MusicNoteTagsOverlay extends StatelessWidget {
  const MusicNoteTagsOverlay({
    super.key,
    required this.tagList,
    required this.onChangeTag,
  });

  final List<TagCategory> tagList;
  final void Function(Tag) onChangeTag;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 700,
      height: 400,
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: Colors.grey[400]!, blurRadius: 1, spreadRadius: 0.5),
        ],
        borderRadius: BorderRadius.circular(5),
      ),
      child: ListView.builder(
          itemCount: tagList.length + 1,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Container(
                alignment: Alignment.centerLeft,
                child: TagItem(
                  t: Tag(tagId: 1000001635, tagName: '经典老歌'),
                  onSelect: onChangeTag,
                ),
              );
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tagList[index - 1].categoryName,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey[600],
                    fontWeight: FontWeight.normal,
                    decoration: TextDecoration.none,
                  ),
                ),
                Wrap(
                    children: tagList[index - 1]
                        .tags
                        .map((t) => TagItem(
                              t: t,
                              onSelect: onChangeTag,
                            ))
                        .toList())
              ],
            );
          }),
    );
  }
}
