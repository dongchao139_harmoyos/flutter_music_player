import 'dart:async';

import 'package:flutter/material.dart';

import 'volume_indicator.dart';
import 'package:audioplayers/audioplayers.dart';

class VolumeIcon extends StatefulWidget {
  const VolumeIcon({
    super.key,
    required this.icon,
    required this.player,
  });
  final Icon icon;
  final AudioPlayer player;

  @override
  State<VolumeIcon> createState() => _VolumeIconState();
}

class _VolumeIconState extends State<VolumeIcon> {
  OverlayEntry? overlayEntry;
  Timer? timer;
  double volume = 1.0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 35,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        onEnter: (event) {
          showFlatingOverlay(context, volume);
        },
        onExit: (event) {
          hideFloatingOverlay(context);
        },
        child: IconButton(
          icon: widget.icon,
          onPressed: () {},
        ),
      ),
    );
  }

  void showFlatingOverlay(BuildContext context, double value) {
    timer?.cancel();
    timer = null;
    if (overlayEntry != null) {
      return;
    }
    var overlayState = Overlay.of(context);
    overlayEntry = OverlayEntry(
        builder: (context) => VolumnIndicator(
            value: value,
            onEnter: (e) {
              timer?.cancel();
              timer = null;
            },
            onExit: (e) {
              hideFloatingOverlay(context);
            },
            seekCallback: (progress) {
              setState(() {
                volume = progress / 100.0;
              });
              if (widget.player.state == PlayerState.playing) {
                widget.player.setVolume(volume);
              }
            }));
    overlayState.insert(overlayEntry!);
  }

  void hideFloatingOverlay(BuildContext context) {
    timer = Timer(const Duration(milliseconds: 200), () {
      overlayEntry?.remove();
      overlayEntry = null;
    });
  }
}
