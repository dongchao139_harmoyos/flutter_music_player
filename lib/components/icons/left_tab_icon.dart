import 'package:flutter/material.dart';

class TabIcon extends StatelessWidget {
  const TabIcon(this.iconData, {super.key, required this.selected, required this.message, required this.onTap});
  final bool selected;
  final String message;
  final IconData iconData;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
        color: Colors.lightBlue[100], border: Border(left: BorderSide(width: 1.5, color: Colors.lightBlue[500]!)));
    if (!selected) {
      boxDecoration = BoxDecoration(border: Border.all(width: 0, color: const Color(0x00000000)));
    }
    return Container(
      width: 70,
      height: 60,
      decoration: boxDecoration,
      child: Tooltip(
        message: message,
        child: IconButton(
          icon: Icon(
            iconData,
            size: 30,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: () {
            onTap();
          },
          style: ButtonStyle(
            shape: MaterialStateProperty.resolveWith((states) {
              return const RoundedRectangleBorder(
                side: BorderSide(width: 0, color: Color(0x00000000)),
              );
            }),
          ),
        ),
      ),
    );
  }
}
