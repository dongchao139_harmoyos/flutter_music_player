import 'package:flutter/material.dart';

import '../../blocs/my_app_bloc.dart';
import '../../models/music_model.dart';

class PlayModeIcon extends StatelessWidget {
  const PlayModeIcon({super.key, required this.mode, required this.bloc});
  final PlayingMode mode;
  final MusicListBloc bloc;

  @override
  Widget build(BuildContext context) {
    var icon = getPlayModeIcon(mode, context);
    var message = getMessage(mode);
    return SizedBox(
      width: 35,
      child: Tooltip(
        message: message,
        child: IconButton(
          onPressed: () => bloc.changeMode(),
          icon: icon,
          iconSize: 20,
        ),
      ),
    );
  }

  Icon getPlayModeIcon(PlayingMode mode, BuildContext context) {
    final color = Theme.of(context).primaryColor;
    if (mode == PlayingMode.sequence) {
      return Icon(Icons.repeat_outlined, color: color);
    }
    if (mode == PlayingMode.cycleOne) {
      return Icon(Icons.repeat_one, color: color);
    }
    if (mode == PlayingMode.random) {
      return Icon(Icons.shuffle, color: color);
    }
    throw Error();
  }

  String getMessage(PlayingMode mode) {
    if (mode == PlayingMode.sequence) {
      return "顺序播放";
    }
    if (mode == PlayingMode.cycleOne) {
      return "单曲循环";
    }
    if (mode == PlayingMode.random) {
      return "随机播放";
    }
    throw Error();
  }
}
