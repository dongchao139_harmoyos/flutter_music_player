import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

import '../../models/music_model.dart';

class PlayIcon extends StatefulWidget {
  const PlayIcon({
    super.key,
    required this.model,
    required this.player,
  });
  final MusicModel model;
  final AudioPlayer player;

  @override
  State<PlayIcon> createState() => _PlayIconState();
}

class _PlayIconState extends State<PlayIcon> {
  bool playing = false;
  late StreamSubscription<PlayerState> stream;
  @override
  void initState() {
    super.initState();
    playing = widget.player.state == PlayerState.playing;
    stream = widget.player.onPlayerStateChanged.listen((PlayerState state) {
      if (state == PlayerState.playing) {
        setState(() {
          playing = true;
        });
      }
      if (state == PlayerState.completed || state == PlayerState.paused) {
        setState(() {
          playing = false;
        });
      }
    });
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: changePlayingState,
        icon: playing
            ? Icon(
                Icons.pause,
                size: 40,
                color: Theme.of(context).primaryColor,
              )
            : Icon(Icons.play_arrow, size: 40, color: Theme.of(context).primaryColor));
  }

  void changePlayingState() {
    if (!playing) {
      if (widget.player.state == PlayerState.paused) {
        widget.player.resume();
      } else {
        if (widget.model.file != null) {
          widget.player.play(DeviceFileSource(widget.model.file!.path));
        } else {
          widget.player.play(UrlSource(widget.model.musicUrl));
        }
        widget.player.setVolume(1.0);
      }
    } else {
      widget.player.pause();
    }
  }
}
