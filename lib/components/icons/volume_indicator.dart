import 'package:flutter_advanced_seekbar/flutter_advanced_seekbar.dart';
import 'package:flutter/material.dart';

class VolumnIndicator extends StatelessWidget {
  const VolumnIndicator(
      {super.key, required this.value, required this.seekCallback, required this.onEnter, required this.onExit});
  final void Function(PointerEvent) onEnter;
  final void Function(PointerEvent) onExit;
  final double value;
  final void Function(int) seekCallback;

  @override
  Widget build(BuildContext context) {
    return Positioned(
        width: 240,
        height: 30,
        bottom: 60,
        right: 100,
        child: MouseRegion(
          onEnter: onEnter,
          onExit: onExit,
          child: Container(
            width: 240,
            height: 30,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey[300]!),
              borderRadius: const BorderRadius.all(Radius.circular(5)),
            ),
            child: AdvancedSeekBar(
              Colors.grey[300]!,
              10,
              Colors.blue,
              defaultProgress: int.parse((value * 100).toStringAsFixed(0)),
              seekBarFinished: (progress) {
                seekCallback(progress);
              },
              fillProgress: true,
            ),
          ),
        ));
  }
}
