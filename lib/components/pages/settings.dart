import 'dart:convert';
import 'package:flutter/material.dart';
import '../../api/migu/api.dart';

class Settings extends StatefulWidget {
  const Settings({super.key, required this.miguPlaylistApi});
  final MiguPlaylistApi miguPlaylistApi;

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool qrCodeOpen = false;
  bool login = false;
  String qrCode = "";

  @override
  void initState() {
    super.initState();
    widget.miguPlaylistApi.loginCallback = () {
      setState(() {
        qrCodeOpen = false;
        login = true;
      });
    };
    widget.miguPlaylistApi.qrLogin().then((qrCode) {
      setState(() {
        qrCodeOpen = true;
        this.qrCode = qrCode;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        qrCodeOpen
            ? Column(children: [
                const Text("请扫码登录"),
                SizedBox(
                  width: 200,
                  height: 200,
                  child: Image.memory(const Base64Decoder().convert(qrCode)),
                ) 
            ],)
            : (login ? const Text("登录成功") : const Text("请先登录"))
      ],
    );
  }
}
