import 'package:flutter/material.dart';

import '../../api/api.dart';
import '../../api/migu/api.dart';
import '../note/music_note_list.dart';
import '../note/music_note_tag_overlay.dart';
import '../../models/music_note_model.dart';

// 歌单
class MusicNote extends StatefulWidget {
  const MusicNote({
    super.key,
    required this.onTapItem, required this.miguPlaylistApi,
  });
  final Function(MusicNoteModel) onTapItem;
  final MiguPlaylistApi miguPlaylistApi;

  @override
  State<MusicNote> createState() => _MusicNoteState();
}

class _MusicNoteState extends State<MusicNote> {
  Tag tag = Tag(tagId: 1000001635, tagName: "经典老歌");
  List<MusicNoteModel> musicNoteList = [];
  List<TagCategory> tagList = [];
  OverlayEntry? _overlayEntry;
  late bool _overlayOpen;

  @override
  void initState() {
    super.initState();
    _overlayOpen = false;
    widget.miguPlaylistApi.requestTags().then((tags) {
      setState(() {
        tagList = tags;
      });
    });
    updatePlaylist();
  }

  void updatePlaylist() {
    widget.miguPlaylistApi.requestPlaylistList(tag.tagId).then((value) {
      setState(() {
        musicNoteList = value.map((e) {
          return MusicNoteModel(
            e.playlistId,
            imageUrl: e.coverImgUrl,
            title: e.playlistTitle,
            author: '咪咕官方歌单',
            date: DateTime.now(),
            startAmount: e.playedCount,
            playAmount: 10000,
          );
        }).toList();
      });
    });
  }

  void _openOverlay() {
    if (_overlayOpen) {
      if (_overlayEntry != null) {
        _overlayEntry!.remove();
        setState(() {
          _overlayOpen = false;
        });
      }
    } else {
      _overlayEntry = _createOverlayEngry();
      Overlay.of(context).insert(_overlayEntry!);
      setState(() {
        _overlayOpen = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Row(
            children: [
              Text(tag.tagName, style: TextStyle(color: Theme.of(context).primaryColor)),
              IconButton(
                onPressed: _openOverlay,
                icon: _overlayOpen ? const Icon(Icons.arrow_upward) : const Icon(Icons.arrow_downward),
              )
            ],
          ),
        ),
        Expanded(
          child: MusicNoteList(
            musicNoteList: musicNoteList,
            onTapItem: widget.onTapItem,
            padding: const EdgeInsets.only(left: 15, right: 15),
          ),
        )
      ],
    );
  }

  OverlayEntry _createOverlayEngry() {
    return OverlayEntry(builder: (context) {
      return Positioned(
        left: 85,
        top: 90,
        child: MusicNoteTagsOverlay(
          tagList: tagList,
          onChangeTag: handleChangeTag,
        ),
      );
    });
  }

  void handleChangeTag(Tag tag) {
    this.tag = tag;
    updatePlaylist();
    _overlayEntry!.remove();
    setState(() {
      this.tag = tag;
      _overlayOpen = false;
    });
  }
}
