import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../api/api.dart';
import '../../api/migu/api.dart';
import '../../blocs/my_app_bloc.dart';
import '../../models/music_model.dart';
import '../../models/music_search_model.dart';
import '../search/search_table.dart';
import '../search/search_table_row.dart';

class Leaderborad extends StatefulWidget {
  const Leaderborad({super.key, required this.miguPlaylistApi});
  final MiguPlaylistApi miguPlaylistApi;

  @override
  State<Leaderborad> createState() => _LeaderboradState();
}

class _LeaderboradState extends State<Leaderborad> {
  List<Board> leaderboardList = [];
  List<String> sourceList = [];
  List<Song> songList = [];
  Board currentBoard = Board(name: "尖叫新歌榜", url: "/v3/music/top/jianjiao_newsong");
  String currentSource = "咪咕音乐";
  OverlayEntry? _overlayEntry;
  bool _overlayOpen = false;
  @override
  void initState() {
    super.initState();
    sourceList = ["咪咕音乐", "酷狗音乐", "酷我音乐", "QQ音乐", "网易云音乐"];
    leaderboardList = [];
    widget.miguPlaylistApi.requestBoardList().then((value) {
      setState(() {
        leaderboardList = value;
      });
    });
    widget.miguPlaylistApi.requestSongListByBoard("/v3/music/top/jianjiao_newsong").then((value) {
      setState(() {
        songList = value;
      });
    });
  }

  void _openOverlay() {
    if (_overlayOpen) {
      if (_overlayEntry != null) {
        _overlayEntry!.remove();
        setState(() {
          _overlayOpen = false;
        });
      }
    } else {
      _overlayEntry = _createOverlayEngry();
      Overlay.of(context).insert(_overlayEntry!);
      setState(() {
        _overlayOpen = true;
      });
    }
  }

  OverlayEntry _createOverlayEngry() {
    return OverlayEntry(builder: (context) {
      return Positioned(
        left: 85,
        top: 90,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[400]!, blurRadius: 1, spreadRadius: 0.5),
            ],
            borderRadius: BorderRadius.circular(5),
          ),
          child: Column(
            children: sourceList.map((e) {
              return TextButton(
                onPressed: () {
                  _overlayEntry!.remove();
                  // setState(() {
                  //   currentSource = e;
                  //   _overlayOpen = false;
                  // });
                },
                child: Text(
                  e,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 14),
                ),
              );
            }).toList(),
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MusicListBloc, MusicListModel>(
        builder: (context, state) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
              children: [
                MouseRegion(
                  cursor: SystemMouseCursors.click,
                  child: GestureDetector(
                    onTap: _openOverlay,
                    child: SizedBox(width: 130, child: Text(currentSource)),
                  ),
                ),
                _overlayOpen
                    ? const Icon(Icons.arrow_upward)
                    : const Icon(Icons.arrow_downward),
                const SearchTableRow(
                  tableCells: ["歌曲名", "艺术家", "专辑名", "时长"],
                  rowIndex: -1,
                  totalWidth: 800,
                )
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Container(
                  width: 160,
                  padding: const EdgeInsets.only(left: 15),
                  child: ListView.builder(
                    itemCount: leaderboardList.length,
                    itemBuilder: (context, index) {
                      var item = leaderboardList[index];
                      var color = Theme.of(context).primaryColor;
                      if (item.name == currentBoard.name) {
                        color = Theme.of(context).hintColor;
                      }
                      return TextButton(
                            child: Text(
                              item.name,
                              style: TextStyle(color: color),
                            ),
                            onPressed: () {
                              setState(() {
                                currentBoard = item;
                              });
                              widget.miguPlaylistApi.requestSongListByBoard(item.url).then((value) {
                                setState(() {
                                  songList = value;
                                });
                              });
                            });
                    },
                  ),
                ),
                Expanded(
                  child: SearchTable(
                    musicList: songList
                        .map((e) => MusicSearchModel(
                              musicName: e.title,
                              author: e.singer,
                              albumn: e.album,
                              musicLength: "04:29",
                              imageUrl: e.imgUrl,
                            ))
                        .toList(),
                    widthIcon: false,
                    widthBorder: true,
                    totalWidth: 800,
                    onTap: (index) {
                      var linkUrl = songList[index].linkUrl;
                      var copyrightId = linkUrl.substring(linkUrl.lastIndexOf('/') + 1);
                      widget.miguPlaylistApi.requestPlayInfoAndPlay(copyrightId: copyrightId).then((playUrl) {
                        context.read<MusicListBloc>().addMusic(MusicModel(
                              musicName: songList[index].title,
                              imageUrl: "http:${songList[index].imgUrl}",
                              musicUrl: playUrl,
                        ));
                      });
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      );
    });
  }
}
