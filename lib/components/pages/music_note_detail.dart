import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../api/api.dart';
import '../../api/migu/api.dart';
import '../../blocs/my_app_bloc.dart';
import '../../models/music_model.dart';
import '../../models/music_note_model.dart';
import '../../models/music_search_model.dart';
import '../search/search_table.dart';

class MusicNoteDetail extends StatefulWidget {
  const MusicNoteDetail(
      {super.key, required this.onBack, required this.model, required this.miguPlaylistApi});
  final VoidCallback onBack;
  final MusicNoteModel model;
  final MiguPlaylistApi miguPlaylistApi;

  @override
  State<MusicNoteDetail> createState() => _MusicNoteDetailState();
}

class _MusicNoteDetailState extends State<MusicNoteDetail> {
  List<Song> songList = [];

  @override
  void initState() {
    super.initState();
    widget.miguPlaylistApi.requestSongList(widget.model.playlistId).then((value) {
      setState(() {
        songList = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MusicListBloc, MusicListModel>(
        builder: (context, state) {
        return Container(
          color: Colors.white,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Image.network(
                        widget.model.imageUrl,
                        height: 80,
                        width: 80,
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.model.title,
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            widget.model.author,
                            style: const TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        TextButton(onPressed: widget.onBack, child: const Text("播放")),
                        TextButton(onPressed: widget.onBack, child: const Text("收藏")),
                        TextButton(onPressed: widget.onBack, child: const Text("返回")),
                      ],
                    )
                  ],
                ),
              ),
              Expanded(
                child: SearchTable(
                  musicList: songList
                      .map((e) => MusicSearchModel(
                            musicName: e.title,
                            author: e.singer,
                            albumn: e.album,
                            musicLength: "04:29",
                            imageUrl: "http:${e.imgUrl}",
                          ))
                      .toList(),
                  widthIcon: true,
                  widthBorder: true,
                  currentIndex: 0,
                  totalWidth: 800,
                  onTap: (index) {
                    var linkUrl = songList[index].linkUrl;
                    var copyrightId = linkUrl.substring(linkUrl.lastIndexOf('/') + 1);
                    widget.miguPlaylistApi.requestPlayInfoAndPlay(copyrightId: copyrightId).then((playUrl) {
                            context.read<MusicListBloc>().addMusic(MusicModel(
                              musicName: songList[index].title,
                              imageUrl: "http:${songList[index].imgUrl}",
                              musicUrl: playUrl,
                            ));
                          });
                  },
                ),
              )
            ],
          ),
        );
      }
    );
  }
}
