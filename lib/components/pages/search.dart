import 'package:flutter/material.dart';

import '../../mock/mock_music_note_list.dart';
import '../../mock/mock_search_list.dart';
import '../../models/music_note_model.dart';
import '../../models/music_search_model.dart';
import '../search/search_tab_item.dart';
import '../search/search_table.dart';
import '../search/search_table_row.dart';

class Search extends StatefulWidget {
  const Search({
    super.key,
    required this.searchText,
    required this.onTapItem,
  });
  final String? searchText;
  final Function(int) onTapItem;

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  String text = '星星点灯';
  late List<MusicSearchModel> musicList;
  late List<MusicNoteModel> musicNoteList;
  String source = "咪咕音乐";
  String mode = "歌曲";

  List<String> sourceList = ["咪咕音乐", "酷狗音乐", "酷我音乐", "QQ音乐", "网易云音乐"];

  @override
  void initState() {
    super.initState();
    musicList = musics;
    musicNoteList = musicNotes;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.searchText != null) {
      text = "/ search${widget.searchText!}";
    }
    List<Widget> list = [];
    List<Widget> list1 = sourceList
        .map((s) => SearchTabItem(
            text: s,
            selected: source == s,
            index: 0,
            onTap: () => {}))
        .toList();
    list.addAll(list1);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: list,
        ),
        mode == "歌曲"
            ? const Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: SearchTableRow(tableCells: ["歌曲名", "艺术家", "专辑名", "时长"], rowIndex: -1),
              )
            : const Row(),
        Expanded(
            child: SearchTable(musicList: musicList),
        )
      ],
    );
  }
}
