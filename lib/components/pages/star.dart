import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_music_player/components/search/search_table.dart';
import 'package:flutter_music_player/models/music_model.dart';
import 'package:flutter_music_player/models/music_search_model.dart';

import '../../blocs/my_app_bloc.dart';
import '../search/search_table_row.dart';

class Star extends StatefulWidget {
  const Star({super.key});

  @override
  State<Star> createState() => _StarState();
}

class _StarState extends State<Star> {
  int hoverIndex = -1;
  late Color bgColor;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MusicListBloc, MusicListModel>(
      builder: (context, state) {
        List<MusicModel> musicList = state.musicList;
        int current = state.current;
        return Column(
          children: [
            const Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                children: [
                  SizedBox(width: 140, child: Text("我的列表")),
                  SearchTableRow(
                    tableCells: ["歌曲名", "艺术家", "专辑名", "时长"],
                    rowIndex: -1,
                    totalWidth: 800,
                  )
                ],
              ),
            ),
            Expanded(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 150,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextButton(
                        onPressed: () => _changeDirectory(context, context.read<MusicListBloc>()),
                        child: const Text("选择文件夹"),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          state.directoryPath,
                          style: const TextStyle(fontSize: 12),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: SearchTable(
                    musicList: musicList
                        .map((e) => MusicSearchModel(
                              musicName: e.musicName,
                              author: e.musicName,
                              albumn: e.musicName,
                              musicLength: "04:29",
                              imageUrl: null,
                            ))
                        .toList(),
                    widthIcon: true,
                    widthBorder: true,
                    currentIndex: current,
                    totalWidth: 800,
                    onTap: (index) {
                      context.read<MusicListBloc>().play(index);
                      context.read<MusicListBloc>().restartPlay();
                    },
                  ),
                )
              ],
            )),
          ],
        );
      },
    );
  }

  void _changeDirectory(BuildContext context, MusicListBloc bloc) {
    getDirectoryPath().then((directoryPath) {
      if (directoryPath == null) {
        return;
      }
      context.read<MusicListBloc>().changeDirectory(directoryPath);
    });
  }
}
