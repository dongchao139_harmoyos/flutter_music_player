import 'package:flutter/material.dart';
import '../../models/music_liric.dart';

class MusicLiricWidget extends StatelessWidget {
  MusicLiricWidget({super.key, this.liric, required this.current});
  final MusicLiric? liric;
  final Duration current;
  final ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    if (liric == null) {
     return const Placeholder();
    }
    var defaultTextStyle = TextStyle(fontSize: 14, color: Colors.grey[400]);
    return ListView.builder(
      controller: _controller, 
      itemCount: liric!.items.length,
      itemBuilder:(context, index) {
        double paddingTop = 10;
        if (index == 0) {
          paddingTop = 210;
        }
        double paddingBottom = 10;
        if (index == liric!.items.length - 1) {
          paddingBottom = 210;
        }
        var text = liric!.items[index].text;
        var textStyle = defaultTextStyle;
        var start = liric!.items[index].enter;
        var end = const Duration(hours: 1);
        if (index < liric!.items.length - 1) {
          end = liric!.items[index + 1].enter;
        }
        if (current.compareTo(start) >= 0 && current.compareTo(end) < 0) {
          textStyle = TextStyle(fontSize: 18, color: Theme.of(context).primaryColor);
          if (index != 0) {
          _controller.jumpTo(index * 40.0);
          }
        }
        return Padding(
          padding: EdgeInsets.only(top: paddingTop, bottom: paddingBottom),
          child: Text(text, textAlign: TextAlign.center, style: textStyle,),
        );
      },
    );
  }
}