import 'package:flutter/material.dart';
import 'package:flutter_lyric/lyrics_reader.dart';
import 'package:flutter_lyric/lyrics_reader_model.dart';

import 'ui_lyric.dart';


class MusicLyricsWidget extends StatelessWidget {
  const MusicLyricsWidget({
    super.key,
    required LyricsReaderModel? lyricsReaderModel,
    required this.duration,
    required UILyric lyricUI,
    required this.onProgress,
  })  : _lyricsReaderModel = lyricsReaderModel,
        _lyricUI = lyricUI;

  final LyricsReaderModel? _lyricsReaderModel;
  final Duration duration;
  final UILyric _lyricUI;
  final void Function(int) onProgress;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        LyricsReader(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          model: _lyricsReaderModel,
          position: duration.inMilliseconds,
          lyricUi: _lyricUI,
          playing: true,
          size: Size(double.infinity, MediaQuery.of(context).size.height / 1.5),
          emptyBuilder: () => Center(
            child: Text(
              "No lyrics",
              style: _lyricUI.getOtherMainTextStyle(),
            ),
          ),
          selectLineBuilder: (progress, confirm) {
            return Row(
              children: [
                IconButton(
                    onPressed: () {
                      LyricsLog.logD("点击事件");
                      confirm.call();
                      onProgress(progress);
                    },
                    icon: const Icon(Icons.play_arrow, color: Colors.green)),
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(color: Colors.green),
                    height: 1,
                    width: double.infinity,
                  ),
                ),
                Text(
                  progress.toString(),
                  style: const TextStyle(color: Colors.green),
                )
              ],
            );
          },
        )
      ],
    );
  }
}
