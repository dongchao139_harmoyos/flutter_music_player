import 'package:flutter/material.dart';

import '../../models/music_search_model.dart';
import 'search_table_row.dart';

class SearchTable extends StatefulWidget {
  const SearchTable({
    super.key,
    required this.musicList,
    this.onTap,
    this.widthIcon = false,
    this.widthBorder = false,
    this.widthImg = false,
    this.hover = true,
    this.currentIndex,
    this.totalWidth = 1010,
  });
  final List<MusicSearchModel> musicList;
  final void Function(int)? onTap;
  final bool widthIcon;
  final bool widthBorder;
  final bool widthImg;
  final bool hover;
  final int? currentIndex;
  final double totalWidth;

  @override
  State<SearchTable> createState() => _SearchTableState();
}

class _SearchTableState extends State<SearchTable> {
  int hoverIndex = -1;
  late Color bgColor;
  @override
  void initState() {
    super.initState();
    bgColor = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.musicList.length,
      itemBuilder: (context, index) {
        var item = widget.musicList[index];
        var row = _row(index, item, widget.currentIndex == index);
        if (widget.widthIcon) {
          var icon = widget.currentIndex == index
              ? Padding(
                  padding: const EdgeInsets.only(left: 5, right: 15),
                  child: Icon(Icons.play_arrow, color: Theme.of(context).hintColor, size: 20),
                )
              : const Padding(
                  padding: EdgeInsets.only(left: 7, right: 15),
                  child: Icon(Icons.stop, color: Colors.grey, size: 18),
                );
          return Row(children: [Expanded(child: row), icon]);
        }
        return row;
      },
    );
  }

  Widget _row(int index, MusicSearchModel item, bool playing) {
    final List<String> cells = item.imageUrl != null ? 
      [item.musicName, item.author, item.albumn, item.musicLength, item.imageUrl!]
      : [item.musicName, item.author, item.albumn, item.musicLength];
    return MouseRegion(
      onHover: (event) {
        if (widget.hover) {
          setState(() {
            bgColor = Colors.blue[50]!;
            hoverIndex = index;
          });
        }
      },
      onExit: (event) {
        if (widget.hover) {
          setState(() {
            bgColor = Colors.white;
            hoverIndex = -1;
          });
        }
      },
      child: Container(
        padding: const EdgeInsets.only(top: 7.5, bottom: 7.5),
        decoration: BoxDecoration(
          color: hoverIndex == index ? bgColor : Colors.white,
          border: widget.widthBorder
              ? Border(
                  left: BorderSide(width: 6, color: Colors.blue[50]!),
                )
              : null,
        ),
        alignment: Alignment.centerLeft,
        child: SearchTableRow(
          tableCells: cells,
          rowIndex: index,
          totalWidth: widget.totalWidth,
          onTap: () {
            if (widget.onTap != null) {
              widget.onTap!(index);
            }
          },
          hover: playing || hoverIndex == index,
        ),
      ),
    );
  }
}
