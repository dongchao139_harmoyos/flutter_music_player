import 'package:flutter/material.dart';

class SearchTableRow extends StatelessWidget {
  const SearchTableRow({
    super.key,
    required this.tableCells,
    required this.rowIndex,
    this.onTap,
    this.totalWidth = 1010,
    this.hover = false,
  });

  final List<String> tableCells;
  final int rowIndex;
  final VoidCallback? onTap;
  final double totalWidth;
  final bool hover;

  @override
  Widget build(BuildContext context) {
    final color =
        hover ? Theme.of(context).hintColor : Theme.of(context).primaryColor;
    return Row(children: [
      Container(
          alignment: Alignment.center,
          width: 50.0 / 1010.0 * totalWidth,
          child: Text(
            rowIndex == -1 ? "#" : (rowIndex + 1).toString(),
            style: TextStyle(color: color),
          )),
      MouseRegion(
        cursor: SystemMouseCursors.click,
        child: GestureDetector(
          onTap: onTap,
          child: Container(
              alignment: Alignment.centerLeft,
              width: 450.0 / 1010.0 * totalWidth,
              child: tableCells.length > 4
                  ? Row(
                      children: [
                        Image.network(
                          tableCells[4],
                          width: 50,
                          height: 50,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                            width: 450.0 / 1010.0 * totalWidth - 60,
                            child: Text(tableCells[0],
                                style: TextStyle(color: color))),
                      ],
                    )
                  : Text(tableCells[0], style: TextStyle(color: color))),
        ),
      ),
      Container(
          alignment: Alignment.centerLeft,
          width: 150.0 / 1010.0 * totalWidth,
          child: Text(tableCells[1], style: TextStyle(color: color))),
      Container(
          alignment: Alignment.centerLeft,
          width: 280.0 / 1010.0 * totalWidth,
          child: Text(tableCells[2], style: TextStyle(color: color))),
      Container(
          alignment: Alignment.centerLeft,
          width: 80.0 / 1010.0 * totalWidth,
          child: Text(tableCells[3], style: TextStyle(color: color))),
    ]);
  }
}
