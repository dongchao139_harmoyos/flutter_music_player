import 'package:flutter/material.dart';

class SearchTabItem extends StatelessWidget {
  const SearchTabItem({
    super.key,
    required this.text,
    required this.selected,
    required this.index,
    required this.onTap,
  });

  final String text;
  final bool selected;
  final int index;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
          decoration: selected
              ? const BoxDecoration(border: Border(bottom: BorderSide(width: 1.5, color: Colors.blue)))
              : const BoxDecoration(border: Border(bottom: BorderSide(width: 1.5, color: Colors.transparent))),
          child: Text(
            text,
            style: TextStyle(color: (selected ? Colors.blue : Theme.of(context).primaryColor)),
          ),
        ),
      ),
    );
  }
}
