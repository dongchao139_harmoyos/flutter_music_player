import 'package:flutter/material.dart';

import '../scaffold/title_bar.dart';

class SearchInput extends StatefulWidget {
  const SearchInput({
    super.key,
    required this.focusNode,
    required this.widget,
  });

  final FocusNode focusNode;
  final TitleBar widget;

  @override
  State<SearchInput> createState() => _SearchInputState();
}

class _SearchInputState extends State<SearchInput> {
  final controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      focusNode: widget.focusNode,
      decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Search for somthing...",
          suffixIcon: SizedBox(
            width: 30,
            child: IconButton(
              onPressed: () {
                widget.widget.onSearch(controller.text);
              },
              padding: EdgeInsets.zero,
              icon: const Icon(Icons.search),
              style: ButtonStyle(shape: MaterialStateProperty.resolveWith((states) {
                return const RoundedRectangleBorder(
                  side: BorderSide(color: Color(0x00000000)),
                );
              })),
            ),
          ),
          suffixIconConstraints: const BoxConstraints(minWidth: 30)),
    );
  }
}
