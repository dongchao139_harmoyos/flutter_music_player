import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_music_player/api/migu/api.dart';

import '../../models/music_note_model.dart';
import '../pages/leaderboard.dart';
import '../pages/music_note.dart';
import '../pages/music_note_detail.dart';
import '../pages/search.dart';
import '../pages/settings.dart';
import '../pages/star.dart';
import 'bottom_bar.dart';
import 'left_navs.dart';
import 'title_bar.dart';

typedef WidgetBuilderWithParam = Widget Function(BuildContext context, Map<String, String>? params);

class CustomScaffold extends StatefulWidget {
  const CustomScaffold({
    super.key, required this.api,
  });
  final MiguPlaylistApi api;

  @override
  State<CustomScaffold> createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  String backupAction = "music_note";
  String action = "music_note";
  int noteIndex = 0;
  MusicNoteModel? model;
  Map<String, String>? params;
  late Map<String, Widget Function(dynamic, Map<String, String>?)> routes;
  @override
  void initState() {
    super.initState();
    routes = {
      '/search': (context, Map<String, String>? params) => Search( // 搜索
            searchText: params?["searchText"],
            onTapItem: (index) {
              setState(() {
                noteIndex = index;
                action = "search";
              });
            },
          ),
      '/music_note': (playlistId, Map<String, String>? params) => MusicNote( // 歌单
            miguPlaylistApi: widget.api,
            onTapItem: (item) {
              setState(() {
                model = item;
                action = "music_note_detail";
                backupAction = "music_note";
              });
            }, 
          ),
      '/music_note_detail': (context, Map<String, String>? params) => MusicNoteDetail( // 歌单详情
            miguPlaylistApi: widget.api,
            model: model!,
            onBack: () {
              setState(() {
                action = backupAction;
                backupAction = "search";
              });
            },
          ),
      '/leaderboard': (context, Map<String, String>? params) => Leaderborad(miguPlaylistApi: widget.api), // 排行榜
      '/star': (context, Map<String, String>? params) => const Star(),    // 我的列表
      '/settings': (context, Map<String, String>? params) => Settings(miguPlaylistApi: widget.api), // 设置
    };
  }

  @override
  Widget build(BuildContext context) {
    final color = WindowButtonColors(
      iconNormal: Colors.black,
      iconMouseOver: Colors.white,
      iconMouseDown: Colors.blueGrey,
    );
    WidgetBuilderWithParam builder = (context, Map<String, String>? params) => const Text("404");
    if (routes.containsKey("/$action")) {
      builder = routes["/$action"]!;
    }
    return Container(
      color: Colors.white,
      child: Row(
        children: [
          LeftNavs(
            action: action,
            onTap: (String action) {
              setState(() {
                this.action = action;
              });
            },
          ),
          Expanded(
            child: Column(
              children: [
                TitleBar(
                  color: color,
                  onSearch: (text) {
                    setState(() {
                      action = "search";
                      params = {"searchText": text};
                    });
                  },
                ),
                Expanded(
                  child: builder(context, params),
                ),
                const BottomBar(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
