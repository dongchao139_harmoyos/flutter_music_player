import 'package:flutter/material.dart';
import 'package:flutter_music_player/components/icons/left_tab_icon.dart';

class LeftNavs extends StatelessWidget {
  final void Function(String) onTap;
  final String action;
  const LeftNavs({super.key, required this.onTap, required this.action});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).highlightColor,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            child: Image.asset("images/logo_icon.ico", fit: BoxFit.cover, width: 50, height: 50),
          ),
          // TabIcon(Icons.search, message: "搜索", selected: action == "search", onTap: () {
          //   onTap("search");
          // }),
          TabIcon(Icons.music_note, message: "歌单", selected: action == "music_note", onTap: () {
            onTap("music_note");
          }),
          TabIcon(Icons.leaderboard, message: "排行榜", selected: action == "leaderboard", onTap: () {
            onTap("leaderboard");
          }),
          TabIcon(Icons.star, message: "我的列表", selected: action == "star", onTap: () {
            onTap("star");
          }),
          TabIcon(Icons.settings, message: "设置", selected: action == "settings", onTap: () {
            onTap("settings");
          }),
        ],
      ),
    );
  }
}
