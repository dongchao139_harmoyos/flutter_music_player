import 'package:flutter/material.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';

import '../search/search_input.dart';

class TitleBar extends StatefulWidget {
  const TitleBar({
    super.key,
    required this.color,
    required this.onSearch,
  });

  final void Function(String) onSearch;
  final WindowButtonColors color;

  @override
  State<TitleBar> createState() => _TitleBarState();
}

class _TitleBarState extends State<TitleBar> {
  bool focus = false;
  final FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      setState(() {
        focus = focusNode.hasFocus;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey[300]!, width: 1))),
      height: 50,
      child: WindowTitleBarBox(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MoveWindow(
              child: Container(
                width: 355,
                margin: const EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
                padding: const EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                  color: Colors.blueGrey[50],
                  border: Border.all(color: focus ? Colors.orangeAccent : Colors.blueGrey[300]!),
                  borderRadius: BorderRadius.circular(3),
                ),
                child: SearchInput(focusNode: focusNode, widget: widget),
              ),
            ),
            Expanded(child: MoveWindow()),
            Row(
              children: [MinimizeWindowButton(colors: widget.color), CloseWindowButton(colors: widget.color)],
            )
          ],
        ),
      ),
    );
  }
}
