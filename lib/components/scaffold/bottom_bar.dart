import 'package:flutter/material.dart';
import 'package:flutter_music_player/components/icons/play_icon.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/my_app_bloc.dart';
import '../../models/music_model.dart';
import '../icons/play_mode_icon.dart';
import '../icons/volumn_icon.dart';
import '../image_navigator.dart';

class BottomBar extends StatefulWidget {
  const BottomBar({super.key, this.widthImage = true});
  final bool widthImage;

  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  double sliderValue = 0.0;
  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).primaryColor;
    return Container(
      decoration: BoxDecoration(
          border: Border(top: BorderSide(width: 1, color: Colors.grey[400]!))),
      child: BlocBuilder<MusicListBloc, MusicListModel>(
        builder: (context, state) {
          var mode = state.mode;
          var model = state.musicList[state.current];
          var duration = state.duration;
          var player = state.player;
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              widget.widthImage ? ImageNavigator(model: model) : const Text(""),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Text(
                        model.musicName,
                        style: TextStyle(color: color),
                      ),
                    ),
                    Row(children: [
                      Expanded(
                        child: SizedBox(
                          height: 30,
                          child: Slider(
                            value: duration.inSeconds >= model.totalTime.inSeconds ? 1.0 : duration.inSeconds / model.totalTime.inSeconds.toDouble(),
                            overlayColor: MaterialStateProperty.resolveWith((states) => Colors.transparent),
                            activeColor: Colors.lightBlue[200],
                            onChanged: (value) {
                              player.seek(Duration(seconds: (value * 300).toInt()));
                            },
                          ),
                        ),
                      ),
                      Text(_formatDuration(duration)),
                      const Text(" / "),
                      Text(_formatDuration(model.totalTime)),
                    ]),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              SizedBox(
                width: 35,
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.add_circle, color: color),
                  iconSize: 20,
                ),
              ),
              SizedBox(
                width: 35,
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.speaker_notes_off, color: color),
                  iconSize: 20,
                ),
              ),
              VolumeIcon(
                icon: Icon(Icons.volume_up_sharp, color: color),
                player: player,
              ),
              PlayModeIcon(bloc: context.read<MusicListBloc>(), mode: mode),
              IconButton(
                  onPressed: () {
                    context.read<MusicListBloc>().pre();
                    context.read<MusicListBloc>().restartPlay();
                    context.read<MusicListBloc>().changePosition(Duration.zero);
                  },
                  icon: Icon(Icons.first_page,
                      size: 40, weight: 700, color: color)),
              PlayIcon(
                model: model,
                player: player,
              ),
              IconButton(
                  onPressed: () {
                    context.read<MusicListBloc>().next();
                    context.read<MusicListBloc>().restartPlay();
                    context.read<MusicListBloc>().changePosition(Duration.zero);
                  },
                  icon: Icon(Icons.last_page,
                      size: 40, weight: 700, color: color)),
            ],
          );
        },
      ),
    );
  }
  _formatDuration(Duration duration) {
    return "${duration.inMinutes.toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}";
  }
}
