import 'package:flutter/material.dart';
import 'package:flutter_music_player/blocs/my_app_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'my_scaffold.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late MusicListBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = MusicListBloc();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          useMaterial3: true,
          fontFamily: "思源黑体",
          fontFamilyFallback: const ["微软雅黑"],
          primaryColor: Colors.blueGrey[900],
          highlightColor: Colors.lightBlue[50],
          hintColor: Colors.red[300],
        ),
        home: Scaffold(
          body: CustomScaffold(api: bloc.state.api),
        ),
      ),
    );
  }
}
