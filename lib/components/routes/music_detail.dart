import 'dart:io';

import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_lyric/lyrics_reader.dart';
import 'package:flutter_lyric/lyrics_reader_model.dart';

import '../../blocs/my_app_bloc.dart';
import '../../config.dart';
import '../../models/music_model.dart';
import '../lyric/music_lyrics_widget.dart';
import '../scaffold/bottom_bar.dart';
import '../lyric/ui_lyric.dart';

class MusicDetail extends StatefulWidget {
  const MusicDetail({super.key});

  @override
  State<MusicDetail> createState() => _MusicDetailState();
}

class _MusicDetailState extends State<MusicDetail> {
  LyricsReaderModel? _lyricsReaderModel;
  final UILyric _lyricUI = UILyric(lineGap: 10);

  @override
  void initState() {
    super.initState();
    readFile("music_lyric.lrc").then((value) {
      setState(() {
        _lyricsReaderModel =
            LyricsModelBuilder.create().bindLyricToMain(value).getModel();
      });
    });
  }

  Future<String> readFile(String liricFileName) async {
    final config = await ConfigReader.readMusicList();
    File file = File("${config.defaultPath}\\lrc\\$liricFileName");
    var txt = file.readAsString();
    return txt;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MusicListBloc, MusicListModel>(
        builder: (context, state) {
      var duration = state.duration;
      var player = state.player;
      return Container(
        color: Colors.white,
        child: Column(
          children: [
            WindowTitleBarBox(
              child: Row(
                children: [
                  Expanded(child: MoveWindow()),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text("返回")),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 50),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 80, right: 20, top: 20, bottom: 80),
                      child: Column(
                        children: [
                          Image.asset(
                            "images/music_note.jpg",
                            width: 300,
                            height: 300,
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: MusicLyricsWidget(
                        lyricsReaderModel: _lyricsReaderModel,
                        duration: duration,
                        lyricUI: _lyricUI,
                        onProgress: (progress) {
                          setState(() {
                            player.seek(Duration(milliseconds: progress));
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 30),
              child: BottomBar(widthImage: false),
            ),
          ],
        ),
      );
    });
  }
}