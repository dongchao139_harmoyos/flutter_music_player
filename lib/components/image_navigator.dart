import 'package:flutter/material.dart';
import '../models/music_model.dart';
import 'routes/music_detail.dart';

class ImageNavigator extends StatelessWidget {
  const ImageNavigator({
    super.key,
    required this.model,
  });

  final MusicModel model;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
      child: Container(
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(6)),
          boxShadow: [
            BoxShadow(color: Colors.grey[400]!, blurRadius: 1, spreadRadius: 0.5),
          ],
        ),
        child: MouseRegion(
          cursor: SystemMouseCursors.click,
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return const Scaffold(body: MusicDetail());
                },
              ));
            },
            child: Image.network(
              model.imageUrl,
              height: 56,
              width: 56,
            ),
          ),
        ),
      ),
    );
  }
}
