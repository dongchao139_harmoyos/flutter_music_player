import 'package:system_tray/system_tray.dart';

Future<void> initSystemTray() async {
  final appWindow = AppWindow();
  final tray = SystemTray();
  await tray.initSystemTray(title: "system tray", iconPath: 'images/app_icon.ico');
  final menu = Menu();
  await menu.buildFrom([
    MenuItemLabel(label: "隐藏主界面", onClicked: (item) => appWindow.show()),
    MenuItemLabel(label: "开启桌面歌词", onClicked: (item) => appWindow.hide()),
    MenuItemLabel(label: "锁定桌面歌词", onClicked: (item) => appWindow.close()),
    MenuItemLabel(label: "取消置顶", onClicked: (item) => appWindow.close()),
    MenuItemLabel(label: "退出", onClicked: (item) => appWindow.close()),
  ]);
  await tray.setContextMenu(menu);
  tray.registerSystemTrayEventHandler((eventName) {
    if (eventName == kSystemTrayEventClick) {
      appWindow.show();
    } else if (eventName == kSystemTrayEventRightClick) {
      tray.popUpContextMenu();
    }
  });
}
