import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_music_player/api/migu/api.dart';

import '../config.dart';
import '../models/music_model.dart';

class MusicListBloc extends Cubit<MusicListModel> {
  List<StreamSubscription> streams = [];
  MusicListBloc()
      : super(MusicListModel(
          api: MiguPlaylistApi(),
          musicList: [
            MusicModel(
                musicName: "铃声123456",
                imageUrl: "https://y.qq.com/music/photo_new/T001R300x300M000002znUKc4ST8aR_1.jpg?max_age=2592000",
                musicUrl:
                    "https://dl.stream.qqmusic.qq.com/C4000020wlH60ijh3b.m4a?guid=6339571300&vkey=AFDD23DF8ED4E3767F169028B0F7C7939A21E7370D3277F48375613F8CCB4E299A5187F0967C469354A04C8EEFF5F4054624FC43BFB3DC1C&uin=&fromtag=120032&src=C400003ZVeoN3dUc31.m4a"),
          ],
          current: 0,
          mode: PlayingMode.sequence,
          duration: Duration.zero,
          directoryPath: "",
          player: AudioPlayer(),
        )) {
    streams.add(state.player.onPositionChanged.listen((Duration duration) {
      changePosition(duration);
    }));
    streams.add(state.player.onPlayerComplete.listen((event) {
      completedOne();
      changePosition(Duration.zero);
      restartPlay();
    }));
    readMusicList();
  }

  Future<void> readMusicList() async {
    final config = await ConfigReader.readMusicList();
    changeDirectory(config.defaultPath);
  }

  StreamSubscription<Duration>? _subscription;

  dispose() {
    for (var it in streams) {
      it.cancel();
    }
  }

  void pre() {
    if (state.mode == PlayingMode.random) {
      state.current = Random().nextInt(state.musicList.length);
    } else if (state.current == state.musicList.length - 1) {
      state.current = 0;
    } else {
      state.current++;
    }
    emit(state.clone());
  }

  void completedOne() {
    if (state.mode == PlayingMode.cycleOne) {
      // do nothing
    } else {
      next();
    }
  }

  void changeMode() {
    if (state.mode == PlayingMode.sequence) {
      state.mode = PlayingMode.random;
    } else if (state.mode == PlayingMode.random) {
      state.mode = PlayingMode.cycleOne;
    } else {
      state.mode = PlayingMode.sequence;
    }
    emit(state.clone());
  }

  void next() {
    if (state.mode == PlayingMode.random) {
      state.current = Random().nextInt(state.musicList.length);
    } else if (state.current == state.musicList.length - 1) {
      state.current = 0;
    } else {
      state.current++;
    }
    emit(state.clone());
  }

  void play(int index) {
    state.current = index;
    emit(state.clone());
  }

  void updateList(List<MusicModel> musicList) {
    state.musicList = musicList;
    if (state.current >= musicList.length) {
      state.current = musicList.length - 1;
    }
    emit(state.clone());
  }

  void addMusic(MusicModel musicModel) {
    state.musicList.add(musicModel);
    state.current = state.musicList.length - 1;
    emit(state.clone());
  }

  void changePosition(Duration duration) {
    state.duration = duration;
    emit(state.clone());
  }

  void changeDirectory(String? directoryPath) {
    if (directoryPath == null) {
      return;
    }
    Directory directory = Directory(directoryPath);
    directory.exists().then((exists) {
      if (!exists) {
        return;
      }
      directory
          .list()
          .where((f) {
            return f.path.endsWith("mp3") || f.path.endsWith("flac") || f.path.endsWith("wav");
          })
          .toList()
          .then((filelist) {
            var musicList = filelist.map((file) {
              return MusicModel(
                  musicName: file.path.substring(file.path.lastIndexOf("\\") + 1),
                  imageUrl: "https://y.qq.com/music/photo_new/T001R300x300M000002znUKc4ST8aR_1.jpg?max_age=2592000",
                  musicUrl: "",
                  file: file);
            }).toList();
            updateList(musicList);
            state.directoryPath = directoryPath;
          });
    });
  }


  void restartPlay() {
    state.player.stop();
    final subscription = _subscription;
    if (subscription != null) {
      subscription.cancel();
    }
    var model = state.musicList[state.current];
    if (model.file != null) {
      state.player.play(DeviceFileSource(model.file!.path));
    } else {
      state.player.play(UrlSource(model.musicUrl));
    }
    _subscription = state.player.onDurationChanged.listen((duration) {
      model.totalTime = duration;
    });
    state.player.setVolume(1.0);
  }
}
